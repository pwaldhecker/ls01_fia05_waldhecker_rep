public class ArrayAufgabe4v2 {

	public static void main(String[] args) {
		int[] arr1 = {3,7,12,18,37,42};
        int[] arr2 = {12,13};
        boolean[] arr3 = {false,false};                
        int iinc = 0;
        
        //Print every number in arr1 and check if arr1 contains any number of arr2
        System.out.print("[ ");
        for (int i = 0;i < arr1.length;i ++) {
                System.out.print(arr1[i]+" ");
                iinc = 0;                        
                //Check if arr1 contains any number of arr2 and save in arr3
                while(iinc < arr2.length) {                
                        if (arr2[iinc] == arr1[i]) {
                                arr3[iinc] = true;
                                break;
                        }
                        iinc ++;
                }
                
        }
        System.out.print("]\n");
        
        //Look up numbers and flags                
        for (int i = 0;i < arr2.length;i ++) {
                if (arr3[i]) {
                        System.out.print("Die Zahl "+arr2[i]+" ist in der Ziehung enthalten.\n");                        
                }else{
                        System.out.print("Die Zahl "+arr2[i]+" ist nicht in der Ziehung enthalten.\n");
                }        
                
        }                
  }      
	
}

