import java.util.Arrays;
import java.util.Scanner;

public class ArrayAufgabe3 {

	public static void main(String[] args) {
		//Aufgabe 3
		 Scanner tastatur = new Scanner(System.in);
	        char[] arr1 = new char[5];
	        String input = tastatur.next();
	        tastatur.close();
	        
	        for (int i = input.length()-1; i >= 0;i --) {
	                arr1[i] = input.charAt(i);                        
	        }
	        
	        for (int i = 4; i >= 0;i --) {
	                System.out.print(arr1[i]);                        
	        }
	
	}
}
